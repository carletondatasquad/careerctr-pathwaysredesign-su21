# Career Pathways Data Visualization
Carleton DataSquad  
Authors: Avery Watts '23, Joshua Song '23 
Based on original work by Carissa Mai-Ping Knipe, '14   
Repository: https://bitbucket.org/carletondatasquad/careerctr-pathwaysredesign-su21/src/main/   
* Check Git History for details 

## How to Run The Most Recent Pathways Code on Your Computer
After cloning the repository, navigate to the dist folder inside the repo.  
You must have npm installed. Then run `npm install http-server -g`. Then run `http-server`.  
This allows you to load the JSON files into JavaScript. The site can then be viewed at http://localhost:8080.  

## Content Descriptions
### dist Folder
This folder contains ready to publish files. These files are currently an updated version of the files in Updated2014Code.

### Description of Files in Updated2014Code and dist
- dependencies
    - `d3.v3.min.js`: imported graphing module (version 3)
    - `jquery-1.10.2.min.js`: jQuery files
    - `sankey.js`: imported graphing module specifically for sankey graphs
    - depreciated code
        - contains the original 2014 sankey and pathway_visualizer files
- source
    - `visualizer.css`: stylesheet
    - `data_organizer.js`: coded by DataSquad; takes json from API endpoint and converts the json into the format used by `pathways_visual.js`
    - `pathways_visual.js`: uses `sankey.js` and `d3.v3.min.js` to create the graph
    - `yearSubmission.js`: takes slider and text box input from `index.html`
- `index.html`: example html code into which the graph is embedded
