const submitForm = () => {
    // Get start and end year
    const formStartYear = document.getElementById("formStartYear").value;
	const formEndYear = document.getElementById("formEndYear").value;
    if (!formStartYear.length || !formEndYear.length) {
        window.alert("Please Input Years");
        return false;
    }
    // Validate number
    if (isNaN(parseFloat(formStartYear)) || isNaN(parseFloat(formEndYear))) {
        window.alert("Please Input Number for Years");
        return false;
    }
	startYear = Number(formStartYear);
	endYear = Number(formEndYear);
    if (startYear < 1970 || endYear > 2020) {
        window.alert("Input Years between 1970 and 2020");
        return false;
    } else {
         // Set slider attribute as input years
	    const yearsInArrayString = formStartYear + ","+ formEndYear;
	    regularSlider.setAttribute("years", yearsInArrayString);
        // Redraw Chart
        drawChart(true);
        return true;
    }
}

