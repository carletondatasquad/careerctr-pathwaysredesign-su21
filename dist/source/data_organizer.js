/*
Pulls JSON data from Advance Database API endpoint and modifies the JSON data to fit 
the format needed by pathways_visualizer.js and d3 sankey.
Converts from json0 to json1.

Written by DataSquad: Avery Watts '23, Joshua Song '23.

Repository: https://bitbucket.org/carletondatasquad/careerctr-pathwaysredesign-su21/src/main/

Dependencies: Referenced by pathways_visualizer.js

File History:
- Created July 2021
- Added AdvanceIds Nov 2021
*/

let flipped = false;
const apiBaseUrl =
  "https://staging.wsg-gke.carleton.edu/wp-admin/admin-ajax.php?action=get_pathways_data";

function getJSON(url) {
  return new Promise((resolve) => {
    fetch(url)
      .then(function (response) {
        return response.json();
      })
      .then(function (json0) {
        let json1;
        if (url.includes("majors") && url.includes("categories")) {
          json1 = changeFormat(json0, "placeConnections");
        } else if (url.includes("categories")) {
          json1 = changeFormat(json0, "categoryConnections");
          json1 = flipLinks(json1);
        } else {
          json1 = changeFormat(json0, "categoryConnections");
        }
        //displayJson(json1);
        resolve(json1);
      })
      .catch(function (err) {
        console.log(err);
      });
  });
  //google reject
}

function parseURL() {
  let currURL = window.location.href;
  urlList = currURL.split("/");
  return urlList;
}

function displayJson(json) {
  /*
	JSON Structure

	for json0: 
	nodeList = json0.data, 
	field = 'nodeLabel', 'nodeCode', 'nodeType', 
	'majorConnections' (returns list of dicts), 
	'categoryConnections' (returns list of dicts), 
	'fieldConnections' (returns list of dicts),
	'placeConnections' (returns list of dicts)
	
	for json1: 
	nodeList = json1.nodes, data.links, 
	field = (for nodes): 'node', 'name' (for links): 'source', 'target', 'value'
	*/

  let strJson = JSON.stringify(json);

  let mainContainer = document.getElementById("json");
  let div = document.createElement("div");
  div.innerHTML = strJson;
  mainContainer.appendChild(div);

  let br = document.createElement("br");
  mainContainer.appendChild(br);
}

/*
This function converts json0 to json1.
The first parameter must be json0.
The second, connectionType, can be: 'majorConnections', 'categoryConnections', 'fieldConnections', 'placeConnections'
- connectionType tells which type of connection should be used.
*/
function changeFormat(json0, connectionType) {
  let json1 = { nodes: [], links: [] };
  let nodeList = json0.data;

  for (let i = 0; i < nodeList.length; i++) {
    let name = nodeList[i].nodeLabel;
    specialMajors = [
      "Ecosystem Science",
      "Ethics",
      "Anthropological Linguistics",
      "Biochemistry",
      "Black Cultural Identities & Dance",
      "Classics - Carleton 4-track",
      "Cognitive Studies",
      "Ecosystem Studies",
      "Composition",
      "Earth System Science",
      "Ethnobotany",
      "Ethnomusicology",
      "Eurasian Studies",
      "German",
      "Global Ethics",
      "Greek",
      "Japanese Literature",
      "Human Ecology",
      "Neurolinguistics",
      "Neuroscience",
      "Narrative Arts",
      "Philosophy of Science",
      "Romance Languages & Literature",
      "Self, Community & Interpretatn",
      "Social Theory of Education",
      "Social Psychology",
      "Urban Media",
      "Morgan Major (special)",
      "Morgan Fellow",
      "Undecided"
    ];

    //filters out most special majors
    if (
      name.indexOf("Spec") === -1 &&
      !specialMajors.includes(name) &&
      name.indexOf("minor") === -1 &&
      name.indexOf("(Conc.)") === -1 &&
      name.indexOf("(conc.)") === -1 &&
      (nodeList[i].majorConnections[0].count !== 1 || depth !== 0)
    ) {
      let newNode = {
        node: json1.nodes.length,
        name: name,
        nodeCode: nodeList[i].nodeCode,
        // Alumni Ids
        advanceIds: nodeList[i].advanceIds,
      };
      //includeDict format: {"inList": true/false, "matchingNode": {nodeDict}}
      let includeDict = includesNode(newNode, json1.nodes);
      let added = includeDict.inList;

      if (added === false) {
        json1.nodes.push(newNode);
      } else {
        newNode = includeDict.matchingNode;
      }

      connectionsList = nodeList[i][connectionType];
      for (let j = 0; j < connectionsList.length; j++) {
        let connectedNode = connectionsList[j];
        let returnDict = includesNode(connectedNode, json1.nodes);
        let included = returnDict.inList;
        let nodeFromJ1 = returnDict.matchingNode;

        if (included === true) {
          //then, connectedNode already exists in json0 format
          let connectedNodeNum = nodeFromJ1.node; //nodeFromJ1.node gives the already assigned number of the node
          let linkDict = {
            source: newNode.node,
            target: connectedNodeNum,
            value: connectedNode.count,
          };
          json1.links.push(linkDict);
        } else {
          //add a new node to json1.nodes and a link from the base newNode to the newer newNode1 to json1.links
          let newNode1 = {
            node: json1.nodes.length,
            name: connectedNode.nodeLabel,
            nodeCode: connectedNode.nodeCode,
            // Alumni Ids
            advanceIds: connectedNode.advanceIds,
          };
          let linkDict = {
            source: newNode.node,
            target: newNode1.node,
            value: connectedNode.count,
          };
          json1.nodes.push(newNode1);
          json1.links.push(linkDict);
        }
      }
    }
  }
  return json1;
}

/*
Helper function to see if checkNode is in nodesList <- a list of dicts containing nodes (nodesList must be in json1 format)
returns a dict {inList:boolean, matchingNode:{nodeDict}}
- inList true if checkNode is in nodesList;
- matchingNode returns the nodeDict of the matching nodeDict in nodesList if inList is true; otherwise returns empty
*/
function includesNode(checkNode, nodesList) {
  let returnDict = { inList: false, matchingNode: {} };
  for (let i = 0; i < nodesList.length; i++) {
    let node = nodesList[i];
    if (checkNode.nodeLabel === node.name) {
      returnDict.inList = true;
      returnDict.matchingNode = node;
      break;
    }
  }
  return returnDict;
}

function flipLinks(json1) {
  let linksList = json1.links;
  let newLinksList = [];
  for (let i = 0; i < linksList.length; i++) {
    let linkDict = linksList[i];
    let newLink = {
      source: linkDict.target,
      target: linkDict.source,
      value: linkDict.value,
    };
    newLinksList.push(newLink);
  }
  json1.links = newLinksList;

  flipped = !flipped;

  return json1;
}

function main() {
  urlList = parseURL();
  let page = urlList[3];
  if (page === "test.html") {
    let newJson = getJSON(apiBaseUrl);
    displayJson(newJson);
  }
}

main();
