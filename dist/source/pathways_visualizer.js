/*
Javascript for generating sankey diagrams of career paths. Used by the pathways_visualizer module.

Based on original work by Carissa Mai-Ping Knipe, '14.
Edited by DataSquad: Avery Watts '23, Joshua Song '23.
- updated to query Advance Database, connect to the alumni database, create top level of equal sized nodes, and other improvements

Repository: https://bitbucket.org/carletondatasquad/careerctr-pathwaysredesign-su21/src/main/

Dependencies: d3.v3.min.js, jquery-1.10.2.min.js, sankey.js, data_organizer.js, visualizer.css

File History:
- original creation 2014
- pulled from Carleton pathways site for update July 2021
- update completed November 2021

Can redirect to Alumni Directory using GET and POST method. 
GET shows the advanced Ids in the url. POST requires the user to login after redirection.
*/

let containerWidth;
let containerHeight;

let jsonData;
let layoutValue = 0;
let canDescend = true;
var depth = 0;

let startYear = 1995;
let endYear = 2015;

let margin = { top: 10, right: 10, bottom: 10, left: 10 };

// Draw Sankey Diagram
const drawChart = (redraw) => {
  let units = "alumni";

  let formatNumber = d3.format(",.0f"), // zero decimal places
    format = function (d) {
      return formatNumber(d) + " " + units;
    },
    color = d3.scale.category20();

  if (redraw) {
    d3.selectAll(".link").remove();
    d3.selectAll(".node").remove();
    d3.selectAll("#pathwaysChart svg").remove();
  }

  let url =
    window.location.protocol +
    "//" +
    window.location.host +
    window.location.pathname;
  let urlSearchStr = window.location.search;
  let apiQuery = apiBaseUrl;
  if (urlSearchStr != "") {
    depth = 1;
    // Implement URLSearchParams
    const urlParams = new URLSearchParams(urlSearchStr);
    if (urlParams.has("startYear") && urlParams.has("endYear")) {
      startYear = urlParams.get("startYear");
      endYear = urlParams.get("endYear");
    }
    // Check queryParam and get when necessary
    if (urlParams.has("career") && urlParams.has("major")) {
      let major = urlParams.get("major").replace("%20", " ");
      let career = urlParams.get("career").replace(" ","%20");
      apiQuery =
        apiBaseUrl + "&anchor=MAJOR&majors=" + major + "&categories=" + career;
    } else if (urlParams.has("career")) {
      let career = urlParams.get("career");
      apiQuery = `${apiBaseUrl}&anchor=MAJOR&categories=${career}`;
    } else if (urlParams.has("major")) {
      let major = urlParams.get("major").replace("%20", " ");
      apiQuery = `${apiBaseUrl}&anchor=MAJOR&majors=${major}`;
    }
  } else {
    depth = 0;
    // Get years from Slider
    const yearInfo = document.getElementById("yearSlider");
    const info = yearInfo.getAttribute("years").split(",");
    startYear = info ? Math.floor(info[0]) : 1995;
    endYear = info ? Math.floor(info[1]) : 2015;
  }

  apiQuery = `${apiQuery}&year_low=${startYear}&year_high=${endYear}`;
  getJSON(apiQuery).then(function (jsonData) {
    let sankey = d3
      .sankey()
      .size([containerWidth, containerHeight])
      .nodes(jsonData.nodes)
      .links(jsonData.links)
      .nodeWidth(20)
      .nodePadding(10);

    // If layoutValue is non-zero, we're not looking at the whole map, so
    // we want to optimize the height of the display. To do that, we set the
    // sankey height to something small (100) and call layoutTest to get a
    // new optimal height.
    if (layoutValue) {
      sankey.size([containerWidth, 100]);
      containerHeight = sankey.layoutTest(1);
      chartElement.height(containerHeight + margin.bottom + "px");
      sankey.size([containerWidth, containerHeight]);
    }

    if (flipped) {
      let leftTitle = document.getElementById("leftSideTitle");
      leftTitle.innerHTML = "Career(s)";
      let rightTitle = document.getElementById("rightSideTitle");
      rightTitle.innerHTML = "Major(s)";
    } else {
      let leftTitle = document.getElementById("leftSideTitle");
      leftTitle.innerHTML = "Major(s)";
      let rightTitle = document.getElementById("rightSideTitle");
      rightTitle.innerHTML = "Career(s)";
    }

    // append the svg canvas to the page
    let svg = d3
      .select("#pathwaysChart")
      .append("svg")
      .attr("width", containerWidth + margin.right + "px")
      .attr("height", containerHeight + margin.bottom + "px")
      .attr("id", "pathwaysSVG")
      .style("overflow", "visible")
      .append("g")
      .attr("transform", "translate(1,1)");

    // Set the sankey layout property (0 means don't sort the nodes; higher values will
    // sort the nodes by size.
    sankey.layout(layoutValue); // layoutValue is a global set by the module

    let path = sankey.link();

    // add in the links
    let link = svg
      .append("g")
      .attr("class", "links")
      .selectAll(".link")
      .data(jsonData.links)
      .enter()
      .append("path")
      .attr("class", function (d) {
        if (canDescend) {
          return "link clickable";
        } else {
          return "link";
        }
      })
      .attr("d", path)
      //used Math.max(5, d.dy) to ensure d.dy not a negative
      .style("stroke-width", function (d) {
        if (depth === 0) {
          return d.dy;
          //return Math.min(2, containerHeight / jsonData.nodes.length);
        } else {
          return Math.max(1, d.dy);
        }
      })
      .style()
      .style("stroke", function (d) {
        return (d.color = color(d.source.name.replace(/ .*/, "")));
      })
      .sort(function (a, b) {
        return b.dy - a.dy;
      });

    // add the link titles
    link.append("title").text(function (d) {
      return d.source.name + " → " + d.target.name + "\n" + format(d.value);
    });

    // set the action for clicking on links
    if (canDescend) {
      let localStorage = window.localStorage;
      link.on("click", function () {
        let major = d3.select(this).datum().source.nodeCode;
        let career = d3.select(this).datum().target.nodeCode;
        let majorName = d3.select(this).datum().source.name;
        let careerName = d3.select(this).datum().target.name;
        let redirectionMethod = "POST";
        if (flipped) {
          let locationUrl = `${url}?startYear=${startYear}&endYear=${endYear}&major=${career}&career=${major}`;
          // If nodeCode is number, it means the next click should connect to alumni directory
          if (!isNaN(major)) {
            let advanceIds = d3.select(this).datum().source.advanceIds.join();
            // Saving advanceIds in QueryParam
            // locationUrl += `&advanceIds=${advanceIds}`

            // GET Style Submission
            // locationUrl = `https://apps.carleton.edu/alumni/directory/?ids=${advanceIds}`;

            // POST Style Submission
            // To use POST, window.location should be disabled when redirected so form can be submitted.
            var form = document.createElement("form");
            var elementIds = document.createElement("input");

            form.method = "POST";
            form.action = "https://apps.carleton.edu/alumni/directory/";

            elementIds.name = "ids";
            elementIds.type = "hidden";
            elementIds.value = advanceIds;
            form.appendChild(elementIds);

            document.body.appendChild(form);
            form.submit();
          } else {
            // save title in local storage
            localStorage.setItem("title", `${careerName}, ${majorName}`);

            // Need for POST
            if (redirectionMethod === "POST") {
              window.location = locationUrl;
            }
          }
          // Need for GET
          if (redirectionMethod === "GET") {
            window.location = locationUrl;
          }
        } else {
          let locationUrl = `${url}?startYear=${startYear}&endYear=${endYear}&major=${major}&career=${career}`;
          // If nodeCode is number, it means the next click should connect to alumni directory
          if (!isNaN(career)) {
            let advanceIds = d3.select(this).datum().target.advanceIds.join();
            // Saving advanceIds in QueryParam
            // locationUrl += `&advanceIds=${advanceIds}`

            // GET Style Submission
            // locationUrl = `https://apps.carleton.edu/alumni/directory/?ids=${advanceIds}`;

            // POST Style Submission
            // To use POST, window.location should be disabled when redirected so form can be submitted.
            var form = document.createElement("form");
            var elementIds = document.createElement("input");

            form.method = "POST";
            form.action = "https://apps.carleton.edu/alumni/directory/";

            elementIds.name = "ids";
            elementIds.type = "hidden";
            elementIds.value = advanceIds;
            form.appendChild(elementIds);

            document.body.appendChild(form);
            form.submit();
          } else {
            // save title in local storage
            localStorage.setItem("title", `${majorName}, ${careerName}`);

            // Need for POST
            if (redirectionMethod === "POST") {
              window.location = locationUrl;
            }
          }
          // Need for GET
          if (redirectionMethod === "GET") {
            window.location = locationUrl;
          }
        }
      });
    }

    // add in the nodes
    let node = svg
      .append("g")
      .attr("class", "nodes")
      .selectAll(".node")
      .data(jsonData.nodes)
      .enter()
      .append("g")
      .attr("class", function (d) {
        if (d.x == 0 || canDescend) {
          return "node clickable";
        } else {
          return "node";
        }
      })
      .style("padding-top", "3em")

      //fixed this transformation issue by overriding the sankey.js node.y value
      .attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
      });

    // add the rectangles for the nodes
    node
      .append("rect")
      //Avery added if ... containerHeight/jsonData.nodes.length to create equal sized nodes
      .attr("height", function (d) {
        if (depth === 0) {
          return containerHeight / jsonData.nodes.length;
        } else {
          return d.dy;
        }
      })
      .attr("width", sankey.nodeWidth())
      .style("fill", function (d) {
        if (!d.name) {
          console.log("The node with missing name: ", d);
        }
        if (d && d.name) {
          return (d.color = color(d.name.replace(/ .*/, "")));
        }
      })
      .style("stroke", function (d) {
        return d3.rgb(d.color).darker(2);
      })
      .style("margin-bottom", 25)
      .append("title")
      .text(function (d) {
        return d.name + "\n" + format(d.value);
      });

    // add in the title for the nodes
    node
      .append("text")
      .attr("x", -6)
      .attr("y", function (d) {
        if (depth === 0) {
          return containerHeight / jsonData.nodes.length / 2;
        } else {
          return d.dy / 2;
        }
      })
      .attr("dy", "0.3em")
      .attr("text-anchor", "end")
      .attr("transform", null)
      .text(function (d) {
        return d.name;
      })
      .filter(function (d) {
        return d.x < containerWidth / 2;
      })
      .attr("x", 6 + sankey.nodeWidth())
      .attr("text-anchor", "start");

    // set the action for clicking on nodes
    node.on("click", function () {
      let diagramTitle = document.querySelector("#chartTitle");
      let localStorage = window.localStorage;
      let redirectionMethod = "POST";
      // Left node
      if (d3.select(this).datum().x == 0) {
        let major = d3.select(this).datum().nodeCode;
        let majorName = d3.select(this).datum().name;
        if (flipped) {
          let locationUrl = `${url}?startYear=${startYear}&endYear=${endYear}&career=${major}`;
          // If nodeCode is number, it means the next click should connect to alumni directory
          if (!isNaN(major)) {
            let advanceIds = d3.select(this).datum().advanceIds.join();
            // Saving advanceIds in QueryParam
            // locationUrl += `&advanceIds=${advanceIds}`

            // GET Style Submission
            // locationUrl = `https://apps.carleton.edu/alumni/directory/?ids=${advanceIds}`;

            // POST Style Submission
            // To use POST, window.location should be disabled when redirected so form can be submitted.
            var form = document.createElement("form");
            var elementIds = document.createElement("input");

            form.method = "POST";
            form.action = "https://apps.carleton.edu/alumni/directory/";

            elementIds.name = "ids";
            elementIds.type = "hidden";
            elementIds.value = advanceIds;
            form.appendChild(elementIds);

            document.body.appendChild(form);
            form.submit();
          } else {
            // save title in local storage
            localStorage.setItem("title", `${majorName}`);

            // Need for POST
            if (redirectionMethod === "POST") {
              window.location = locationUrl;
            }
          }
          // Need for GET
          if (redirectionMethod === "GET") {
            window.location = locationUrl;
          }
        } else {
          // save title in local storage
          localStorage.setItem("title", `${majorName}`);
          window.location = `${url}?startYear=${startYear}&endYear=${endYear}&major=${major}`;
        }
      }
      // Right node
      else if (canDescend) {
        let career = d3.select(this).datum().nodeCode;
        let careerName = d3.select(this).datum().name;
        if (flipped) {
          localStorage.setItem("title", `${careerName}`);
          window.location = `${url}?startYear=${startYear}&endYear=${endYear}&major=${career}`;
        } else {
          let locationUrl = `${url}?startYear=${startYear}&endYear=${endYear}&career=${career}`;
          // If nodeCode is number, it means the next click should connect to alumni directory
          if (!isNaN(career)) {
            let advanceIds = d3.select(this).datum().advanceIds.join();
            // Saving advanceIds in QueryParam
            // locationUrl += `&advanceIds=${advanceIds}`

            // GET Style Submission
            // locationUrl = `https://apps.carleton.edu/alumni/directory/?ids=${advanceIds}`;

            // POST Style Submission
            // To use POST, window.location should be disabled when redirected so form can be submitted.
            var form = document.createElement("form");
            var elementIds = document.createElement("input");

            form.method = "POST";
            form.action = "https://apps.carleton.edu/alumni/directory/";

            elementIds.name = "ids";
            elementIds.type = "hidden";
            elementIds.value = advanceIds;
            form.appendChild(elementIds);

            document.body.appendChild(form);
            form.submit();
          } else {
            localStorage.setItem("title", `${careerName}`);

            // Need for POST
            if (redirectionMethod === "POST") {
              window.location = locationUrl;
            }
          }
          // Need for GET
          if (redirectionMethod === "GET") {
            window.location = locationUrl;
          }
        }
      }
    });

    drawCareerResources(sankey, redraw);
  });
};

// Add the links on the right side of each career
const drawCareerResources = (sankey, redraw) => {
  if (redraw) {
    document.getElementById("careerLink").innerHTML = "";
  }

  sankey.nodes().forEach(function (node) {
    if (node.link) {
      let firstDiv = document.createElement("div");
      let xPos = node.x + sankey.nodeWidth() + 5;
      let yPos = node.y;
      if (depth === 0) {
        let height = containerHeight / jsonData.nodes.length;
      } else {
        let height = node.dy;
      }

      firstDiv.setAttribute(
        "style",
        "margin-top:" +
          yPos +
          "px; margin-left:" +
          xPos +
          "px; height:" +
          containerHeight +
          "px"
      );

      firstDiv.innerHTML = node.link;
      document.getElementById("careerLink").appendChild(firstDiv);
    }
  });
};

// Draw chart when DOM is ready
$(document).ready(() => {
  let chartElement = $("#pathwaysChart");

  containerWidth = chartElement.width();
  containerHeight = chartElement.height();

  drawChart();

  function handleResize() {
    if (containerWidth != chartElement.width()) {
      containerWidth = chartElement.width();
      drawChart(true);
    }
  }

  setInterval(handleResize, 500);
});
