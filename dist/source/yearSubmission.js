/*
Recieves inputs from text fields and sliders in index.html. Referenced by 
pathways_visualizer.js to adjust the years the data is being pulled from.

Written by DataSquad: Avery Watts '23, Joshua Song '23.

Repository: https://bitbucket.org/carletondatasquad/careerctr-pathwaysredesign-su21/src/main/

Dependencies: Referenced by pathways_visualizer.js

File History:
- Created October 2021
*/

const submitForm = () => {
    // Get start and end year from form
    const formStartYear = document.getElementById("formStartYear").value;
	const formEndYear = document.getElementById("formEndYear").value;

    // If the form is empty, return false
    if (!formStartYear.length || !formEndYear.length) {
        window.alert("Please Input Years");
        return false;
    }

    // Validate number
    if (isNaN(parseFloat(formStartYear)) || isNaN(parseFloat(formEndYear))) {
        window.alert("Please Input Number for Years");
        return false;
    }
	startYear = Number(formStartYear);
	endYear = Number(formEndYear);

    // Set Boundary
    if (startYear < 1970 || endYear > 2020) {
        window.alert("Input Years between 1970 and 2020");
        return false;
    } else {
         // Set slider attribute as input years
	    const yearsInArrayString = formStartYear + ","+ formEndYear;
	    regularSlider.setAttribute("years", yearsInArrayString);
        // Redraw Chart
        drawChart(true);
        return true;
    }
}

